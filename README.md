Requirements.

1. Python 3.x
2. Anaconda 3.x
3. Jupytor notebook 3.x
4. Tableau - Tableau Public is free version.

Todo.

1. Remove credential from dbconnect.py
2. Automation script to setup environment

This repo includes all the work done by the data Science team.

1. dbconnect.py --> connect to database server and pull the data. Data will be stored in output is .csv file.
2. initial_analysis.twb --> Dashboards for quick analysis, build in Tableau.
3. transaction_patterns.ipynb --> attempt to find periodcity in inflow and outflow transactions. 'Expense' encapsulates spending and bill both.
4. can-we-predict-month-spend-early.ipynb --> Predict any overspend at the start of the month. With current data volume , the ouput is limited-useful.

																									Created by Shel Zaroo