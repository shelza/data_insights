#!/Users/shel/anaconda3/bin/python

from sqlalchemy import create_engine
import pandas as pd
from pandas import DataFrame

def get_data():
	douugh_api = create_engine('postgresql+psycopg2://douugh_user:6seJVME468caQXNLUeuA7P9XS9NpWxaP@douugh-api-production-db-replica.cszftozkib7s.us-east-1.rds.amazonaws.com/douugh_api')
	data_trans=pd.read_sql("""
SELECT DISTINCT
transaction.id, transaction.location_id,transaction.date,transaction.amount, transaction.name,account.name,account.type,"user".id,
account.subtype,account.account_number, institution.name,external_category.category,external_category.sub_category
FROM
account , transaction, user_institution,"user", institution, category_mapping, external_category
WHERE transaction.account_id = account.id
AND user_institution.id = account.user_institution_id
AND institution.id = user_institution.institution_id
AND user_institution.user_id = "user".id
AND transaction.category_mapping_id = category_mapping.id
AND external_category.id = category_mapping.external_category_id
order by transaction.date""",con=douugh_api)
	data_loc=pd.read_sql("""Select * from location""",con=douugh_api)
	data=data_trans.merge(data_loc,left_on='location_id',right_on='id',how='outer')
	data.drop(['location_id','id_y','address','city','store_number','zip'],axis=1,inplace=True)
	data.columns=['date','amount','description','account_name','account_type','user_id','account_subtype', 'account_number', 'bank_name', 'category','sub_category','lat','long','state']
	return data

if __name__ == '__main__':
	get_data().to_csv('data.csv',index=0)
